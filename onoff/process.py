import logging
import os
import signal
import typing

from .common import OnoffDevice, OnoffState
from .gobject import spawn_child, ScheduledFunction

logger = logging.getLogger("onoff.process")

class OnoffProcess(OnoffDevice):
    """A device that in activated by starting a process and deactivated by
    killing the process.

    @ivar pid: is either None if there is no process or the pid of the
            spawned process
    @ivar starting: is the callback signalling the end of the activation
            transition
    @ivar killed: indicates whether the termination signal has been sent
            to the spawned process.
    """
    pid: typing.Optional[int]
    starting: typing.Optional[ScheduledFunction]
    killed: bool

    def __init__(self, command: typing.List[str],
                 termsig: signal.Signals = signal.SIGTERM) -> None:
        """
        @param command: an argument vector to be executed. The first element
                is used as executable and looked up in $PATH.
        @param termsig: termination signal to be sent to the process to
                deactivate it. The process must exit in response to this
                signal.
        """
        OnoffDevice.__init__(self)
        self.command = command
        self.termsig = termsig
        self.desired_state = OnoffState.inactive
        self.pid = None
        self.killed = False

    @property
    def state(self) -> OnoffState:
        if self.killed:
            return self.desired_state | OnoffState.transition
        return self.desired_state

    def start_process(self) -> None:
        assert self.pid is None
        logger.info("starting command %s", " ".join(self.command))
        self.pid = spawn_child(self.command, self.process_died)
        logger.debug("started as pid %d", self.pid)
        assert self.desired_state == OnoffState.active
        logger.debug("process started")
        self.changestate(self.state)

    def process_died(self, pid: int, condition: int) -> None:
        assert self.pid == pid
        self.pid = None
        self.killed = False
        logger.info("process %d died", pid)
        if self.desired_state == OnoffState.active:
            self.start_process()
        else:
            self.changestate(self.state)

    def activate(self) -> None:
        if self.desired_state != OnoffState.active:
            self.desired_state = OnoffState.active
            if self.pid is None:
                self.start_process()
            else:
                logger.debug("already activated. nothing to do")

    def deactivate(self) -> None:
        if self.desired_state != OnoffState.inactive:
            self.desired_state = OnoffState.inactive
            if self.pid is not None and not self.killed:
                logger.info("killing process %d", self.pid)
                os.kill(self.pid, self.termsig)
                self.killed = True
                self.changestate(self.state)
            else:
                logger.debug("already deactivated. nothing to do.")

    def close(self) -> None:
        self.deactivate()
