import enum
import logging
import typing

from .gobject import ScheduledFunction

logger = logging.getLogger("onoff.common")


class OnoffState(enum.Flag):
    inactive = 0
    active = 1
    deactivating = 2
    activating = 3
    transition = 2


class OnoffDevice:
    """A device is a thing with two states, that can be asked to transition
    from either state to the other. It can signal state changes to interested
    parties.

    @ivar notify: A set of functions taking a changed state.
    """
    notify: typing.Set[typing.Callable[[OnoffState], None]]

    def __init__(self) -> None:
        self.notify = set()

    @property
    def state(self) -> OnoffState:
        """read-only attribute to retrieve the current state"""
        raise NotImplementedError

    def changestate(self, state: OnoffState) -> None:
        """Tell interested parties that the state has changed to the given
        state."""
        for func in self.notify:
            func(state)

    def activate(self) -> None:
        """Ask the device to power on."""
        raise NotImplementedError

    def deactivate(self) -> None:
        """Ask the device to power off."""
        raise NotImplementedError

    def close(self) -> None:
        """Release resources acquired by the constructor."""

class InvertedDevice(OnoffDevice):
    """A device that swaps active and inactive states of a give device."""
    def __init__(self, device: OnoffDevice) -> None:
        OnoffDevice.__init__(self)
        self.device = device
        self.device.activate()
        self.device.notify.add(self.changestate)

    def changestate(self, state: OnoffState) -> None:
        OnoffDevice.changestate(self, state ^ OnoffState.active)

    @property
    def state(self) -> OnoffState:
        return self.device.state ^ OnoffState.active

    def activate(self) -> None:
        self.device.deactivate()

    def deactivate(self) -> None:
        self.device.activate()

    def close(self) -> None:
        self.device.notify.remove(self.changestate)
        self.device.close()

class ThrottledDevice(OnoffDevice):
    """A device that delays the activation signal and the actual deactivation
    by a fixed amounts of time. This limits the rate of state transitions to
    two per offdelay."""
    def __init__(self, device: OnoffDevice, ondelay: float,
                 offdelay: float) -> None:
        """
        @param ondelay: delay the report of OnoffState.active by this many
                        seconds
        @param offdelay: delay the actual deactivation by this many seconds
        """
        OnoffDevice.__init__(self)
        self.device = device
        self.ondelay = ondelay
        self.offdelay = offdelay
        self.desired_state = OnoffState.inactive
        self.transition: typing.Optional[ScheduledFunction] = None
        self.device.notify.add(self.changestate)

    @property
    def state(self) -> OnoffState:
        if self.transition is None:
            return self.device.state
        return self.desired_state | OnoffState.transition

    def _schedule_transition(self, delay: float,
                             func: typing.Callable[[], None]) -> None:
        assert self.transition is None
        self.transition = ScheduledFunction(delay, func)

    def _cancel_transition(self) -> None:
        assert self.transition is not None
        self.transition.cancel()
        self.transition = None

    def changestate(self, state: OnoffState) -> None:
        if state != OnoffState.active:
            OnoffDevice.changestate(self, state)
        else:
            if self.desired_state == OnoffState.inactive:
                logger.warning("device became active, but we want inactive, "
                               "suppresing signal")
            elif self.transition is None:
                logger.debug("scheduling report of activation in %.1fs",
                             self.ondelay)
                self._schedule_transition(self.ondelay, self._report_active)
            else:
                logger.debug("suppressing duplicate activation signal")

    def _report_active(self) -> None:
        assert self.desired_state == OnoffState.active
        assert self.transition is not None
        self.transition = None
        logger.debug("delivering activation signal")
        OnoffDevice.changestate(self, OnoffState.active)

    def activate(self) -> None:
        if self.desired_state == OnoffState.inactive and \
                self.transition is not None:
            logger.debug("cancelling pending deactivation")
            self._cancel_transition()
            curstate = self.device.state
            if curstate == OnoffState.active:
                self.desired_state = OnoffState.active
                OnoffDevice.changestate(self, OnoffState.active)
                return
            logger.warning("device should be active during delayed "
                           "deactivation, but is in state %s", curstate.name)
        self.desired_state = OnoffState.active
        self.device.activate()
        self.changestate(self.device.state)

    def _do_stop(self) -> None:
        assert self.desired_state == OnoffState.inactive
        assert self.transition is not None
        self.transition = None
        logger.debug("actually deactivating")
        self.device.deactivate()

    def deactivate(self) -> None:
        if self.desired_state == OnoffState.active and \
                self.transition is not None:
            logger.debug("cancelling pending activation report")
            self._cancel_transition()
        self.desired_state = OnoffState.inactive
        if self.transition is None:
            logger.debug("scheduling actual deactivate in %.1fs",
                         self.offdelay)
            self._schedule_transition(self.offdelay, self._do_stop)
            self.changestate(self.state)
        else:
            logger.debug("not issuing deactivate due to pending deactivate")

    def close(self) -> None:
        if self.transition is not None:
            logger.info("cancelling pending transition")
            self._cancel_transition()
            if self.desired_state == OnoffState.inactive:
                logger.info("invoking pending deactivate early during close")
                self.device.deactivate()
        self.device.notify.remove(self.changestate)
        self.device.close()
