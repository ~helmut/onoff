import logging
import typing

from .common import OnoffDevice, OnoffState
from .gobject import spawn_child

logger = logging.getLogger("onoff.command")

class OnoffCommand(OnoffDevice):
    """A device that is enabled and disabled by executing separate commands.
    The transition period is the duration of the commands.
    @ivar pid: is either None or the pid of a transition command as long as
            it lives.
    @ivar desired_state: is the state that should be transitioned to
    @ivar target_state: is the state that we are currently transitioning to
    """
    pid: typing.Optional[int]
    desired_state: OnoffState
    target_state: OnoffState

    def __init__(self, oncommand: typing.List[str],
                 offcommand: typing.List[str]) -> None:
        """

        @param oncommand: an argument vector to be executed for activation.
        @param offcommand: an argument vector to be executed for deactivation.
        @note: For both commands the first element is used as executable and
                looked up in $PATH.
        """
        OnoffDevice.__init__(self)
        self.oncommand = oncommand
        self.offcommand = offcommand
        self.desired_state = OnoffState.inactive
        self.pid = None
        self.target_state = OnoffState.inactive  # last command target state

    @property
    def state(self) -> OnoffState:
        if self.pid is not None:
            return self.desired_state | OnoffState.transition
        return self.desired_state

    def transition(self, state: OnoffState) -> None:
        command, name = [(self.offcommand, "offcommand"),
                         (self.oncommand, "oncommand")][state.value]
        self.target_state = state
        logger.info("invoking %s %s", name, " ".join(command))
        self.pid = spawn_child(command, self.process_died)
        logger.debug("started %s as pid %d", name, self.pid)
        self.changestate(self.state)

    def process_died(self, pid: int, condition: int) -> None:
        assert self.pid == pid
        self.pid = None
        logger.info("command %d targeting %s completed", pid,
                    self.target_state.name)
        if self.desired_state == self.target_state:
            self.changestate(self.state)
        else:
            logger.info("desired state changed to %s during invocation "
                        "targeting %s", self.desired_state.name,
                        self.target_state.name)
            self.transition(self.desired_state)

    def activate(self) -> None:
        if self.desired_state != OnoffState.active:
            self.desired_state = OnoffState.active
            if self.pid is None:
                self.transition(self.desired_state)

    def deactivate(self) -> None:
        if self.desired_state != OnoffState.inactive:
            self.desired_state = OnoffState.inactive
            if self.pid is None:
                self.transition(self.desired_state)
