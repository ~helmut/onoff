import typing

from gi.repository import GLib

def spawn_child(command: typing.List[str],
                callback: typing.Callable[[int, int], None]) -> int:
    """Spawn a child process with GLib and attach a callback to the
    termination of the spawned process.

    @param command: an argument vector. First element is used as argv[0] and
            used as executable to look up in $PATH.
    @param callback: is executed with a given pid and condition when the
            command completes
    @returns: the child process pid
    """
    ret = GLib.spawn_async(command,
            flags=GLib.SPAWN_SEARCH_PATH | GLib.SPAWN_DO_NOT_REAP_CHILD)
    pid = ret[0]
    assert isinstance(pid, int)
    assert pid
    GLib.child_watch_add(pid, callback)
    return pid

class ScheduledFunction:
    def __init__(self, interval: float,
                 function: typing.Callable[[], None]) -> None:
        """
        @param interval: seconds
        """
        self.event = GLib.timeout_add(int(1000 * interval), function)

    def cancel(self) -> None:
        ret = GLib.source_remove(self.event)
        assert ret
