import pathlib

from .common import OnoffDevice, OnoffState

class OnoffGPIO(OnoffDevice):
    """A GPIO."""
    def __init__(self, gpionumber: int, active_low: bool = False) -> None:
        """
        @param gpionumber: a Linux GPIO number. That includes a controller
                           offset if necessary.
        @param active_low: whether the GPIO should be inverted
        """
        OnoffDevice.__init__(self)
        self.current_state = OnoffState.inactive
        basedir = pathlib.Path("/sys/class/gpio")
        self.gpiodir = basedir / ("gpio%d" % gpionumber)
        if not self.gpiodir.is_dir():
            (basedir / "export").write_text("%d" % gpionumber)
        (self.gpiodir / "direction").write_text("out")
        (self.gpiodir / "active_low").write_text("%d" % active_low)

    def _set_state(self, state: OnoffState) -> None:
        self.current_state = state
        (self.gpiodir / "value").write_text(
            "1" if state == OnoffState.active else "0")
        self.changestate(self.current_state)

    @property
    def state(self) -> OnoffState:
        return self.current_state

    def activate(self) -> None:
        self._set_state(OnoffState.active)

    def deactivate(self) -> None:
        self._set_state(OnoffState.inactive)
