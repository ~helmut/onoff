import argparse
import logging
import socket
import typing
import xml.parsers.expat

import dbus
import dbus.proxies
import dbus.service
from gi.repository import GLib

from .common import OnoffDevice, OnoffState
from .gobject import ScheduledFunction

logger = logging.getLogger("onoff.dbusutils")

object_prefix = "/de/subdivi/onoff0"
default_busname = "de.subdivi.onoff0"

dbus_options = argparse.ArgumentParser(add_help=False)
dbus_options.add_argument("--bus", default="session",
                          choices=("system", "session"),
                          help="which bus to use (default: %(default)s)")
dbus_options.add_argument("--busname", type=str, default=default_busname,
                          help="which busname (i.e. client) to use "
                               "(default: %(default)s)")
dbus_options.add_argument("--device", type=str,
                          help="which device to control")

def get_dbus(namespace: argparse.Namespace) -> dbus.Bus:
    """
    @param namespace: a namespace returned from a dbus_options argument parser
    @returns: the requested bus
    """
    if namespace.bus == "session":
        return dbus.SessionBus()
    if namespace.bus == "system":
        return dbus.SystemBus()
    raise AssertionError("namespace.bus %r is neither session nor system" %
                         namespace.bus)

def get_dbus_proxy(namespace: argparse.Namespace) -> dbus.proxies.ProxyObject:
    """
    @param namespace: a namespace returned from a dbus_options argument parser
    @returns: a dbus object proxy
    """
    bus = get_dbus(namespace)
    if not namespace.device:
        raise ValueError("no --device given")
    objname = "%s/%s" % (object_prefix, namespace.device)
    return bus.get_object(namespace.busname, objname)

def socketpair() -> typing.Tuple[socket.socket, dbus.types.UnixFd]:
    """Create a socket pair where the latter end is already wrapped for
    transmission over dbus.
    """
    s1, s2 = socket.socketpair()
    s3 = dbus.types.UnixFd(s2)
    s2.close()
    return s1, s3

def list_objects(namespace: argparse.Namespace,
                 path: typing.Optional[str] = None) -> typing.List[str]:
    """List objects on the given bus and busname starting with path. Only the
    trailing components after the slash are returned.

    @param path: prefix for searching objects. Defaults to
        dbusutils.object_prefix.
    @returns: the trailing components of the objects found
    """
    bus = get_dbus(namespace)
    if path is None:
        path = object_prefix
    xmlstring = bus.get_object(namespace.busname, path).Introspect()
    parser = xml.parsers.expat.ParserCreate()
    nodes = []
    def start_element(name: str, attrs: typing.Dict[str, typing.Any]) -> None:
        if name != "node":
            return
        try:
            value = attrs["name"]
        except KeyError:
            return
        nodes.append(value)
    parser.StartElementHandler = start_element
    parser.Parse(xmlstring)
    return nodes

class OnoffControl(dbus.service.Object):
    domain = default_busname
    path = object_prefix

    def __init__(self, bus: dbus.Bus, name: str, device: OnoffDevice) -> None:
        busname = dbus.service.BusName(self.domain, bus=bus)
        dbus.service.Object.__init__(self, busname,
                                     "%s/%s" % (self.path, name))
        self.device = device
        self.usecount = 0
        device.notify.add(self._changestate_notifier)

    def _changestate_notifier(self, state: OnoffState) -> None:
        logger.debug("emitting state %s", state.name)
        self.changestate(state.value)

    @dbus.service.signal(domain, signature="q")
    def changestate(self, state):
        pass

    @dbus.service.method(domain, out_signature="q")
    def state(self):
        return self.device.state.value

    @dbus.service.method(domain, in_signature="q", out_signature="q")
    def activatetime(self, duration):
        """Activate the device for a given number of seconds."""
        logger.info("activatetime %d", duration)
        ScheduledFunction(duration, self.unuse)
        return self.use()

    @dbus.service.method(domain, in_signature="", out_signature="qh")
    def activatefd(self):
        """Activate a device until the returned file descriptor is closed."""
        notifyfd, retfd = socketpair()
        logger.info("activatefd returning fd %d", notifyfd.fileno())
        def callback(fd: socket.socket, _: typing.Any) -> bool:
            logger.info("fd %d completed", fd.fileno())
            fd.close()
            self.unuse()
            return False
        GLib.io_add_watch(notifyfd, GLib.IO_HUP|GLib.IO_ERR, callback)
        return (self.use(), retfd)

    def use(self) -> int:
        self.usecount += 1
        if self.usecount <= 1:
            self.device.activate()
        return self.device.state.value

    def unuse(self) -> None:
        self.usecount -= 1
        if not self.usecount:
            self.device.deactivate()
        else:
            logger.debug("%d users left", self.usecount)
