import typing

from .command import OnoffCommand

def sispmctl_device(sock: int, device: typing.Union[None, int, str] = None) \
        -> OnoffCommand:
    """Create an OnoffCommand that controls the specified socket on a sispmctl
    device.

    @param sock: the socket number of the sispmctl controlled device
    @param device: optional identification of the device. If this is a number,
            it is passed via -d. If it is a string, it is used as a serial and
            passed via -D.
    """
    cmd = ["sispmctl"]
    if isinstance(device, int):
        cmd.extend(["-d", "%d" % device])
    elif isinstance(device, str):
        cmd.extend(["-D", device])
    elif device is not None:
        raise TypeError("passed device must be int, str or None")
    sockstr = "%d" % sock
    return OnoffCommand(cmd + ["-o", sockstr], cmd + ["-f", sockstr])

def uhubctl_device(*, location: typing.Optional[str] = None,
                   port: typing.Optional[int] = None, exact: bool = False,
                   vendor: typing.Optional[str] = None) -> OnoffCommand:
    """Create an OnoffCommand that controls the specified sockets on a
    uhubctl-compatible USB hub.

    @param port: --ports parameter to uhubctl
    @param location: --location parameter to uhubctl
    @param vendor: --vendor parameter to uhubctl
    """
    cmd = ["uhubctl"]
    if port is not None:
        cmd.extend(["--ports", "%d" % port])
    if location is not None:
        cmd.extend(["--location", location])
    if vendor is not None:
        cmd.extend(["--vendor", vendor])
    if exact:
        cmd.append("--exact")
    return OnoffCommand(cmd + ["--action", "1"], cmd + ["--action", "0"])
