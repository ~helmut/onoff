#!/usr/bin/env python3

from distutils.core import setup

setup(name="onoff",
      author="Helmut Grohne",
      author_email="helmut@subdivi.de",
      requires=["dbus",
                "gi.repository",
                "mpd", # optional
               ],
      packages=["onoff"],
      scripts=["dbus_client.py", "dbus_service.py", "mpd_watcher.py"],
)
