#!/usr/bin/env python3
"""
A dbus service example currently providing a device called redshift, that runs
redshift whenever it is not activated. This could be useful to temporarily
disable redshift e.g. while watching a movie.
"""

import logging

import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib

import onoff.common
import onoff.dbusutils
import onoff.process

def main():
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    DBusGMainLoop(set_as_default=True)
    bus = dbus.SessionBus()
    dev = onoff.process.OnoffProcess(["redshift"])
    dev = onoff.common.ThrottledDevice(dev, 3, 0)
    dev = onoff.common.InvertedDevice(dev)
    dev = onoff.common.ThrottledDevice(dev, 1, 5)
    onoff.dbusutils.OnoffControl(bus, "redshift", dev)
    GLib.MainLoop().run()

if __name__ == "__main__":
    main()
