#!/usr/bin/python3
"""
A client to a device given on the commnd line, that is activated whenever the
local mpd plays music.
"""

import argparse
import os
import socket

from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib
import mpd

import onoff.dbusutils

class MpdWatcher:
    def __init__(self, onoffproxy, mpdaddress=("localhost", 6600)):
        self.mpdaddress = mpdaddress
        self.mpdclient = mpd.MPDClient()
        self.onoffproxy = onoffproxy
        self.activatefd = None
        self.statechanged = True
        self.do_connect()

    def handle_write(self, source, condition):
        if self.statechanged:
            self.mpdclient.send_status()
        else:
            self.mpdclient.send_idle()
        GLib.io_add_watch(self.mpdclient, GLib.IO_IN, self.handle_read)
        return False

    def handle_read(self, source, condition):
        try:
            if self.statechanged:
                state = self.mpdclient.fetch_status()["state"]
                self.statechanged = False
                if state == "play":
                    if self.activatefd is None:
                        st, fd = self.onoffproxy.activatefd()
                        self.activatefd = fd.take()
                else:
                    if self.activatefd is not None:
                        os.close(self.activatefd)
                        self.activatefd = None
            else:
                changes = self.mpdclient.fetch_idle()
                if "player" in changes:
                    self.statechanged = True
        except mpd.ConnectionError:
            self.reconnect()
        else:
            GLib.io_add_watch(self.mpdclient, GLib.IO_OUT, self.handle_write)
        return False

    def reconnect(self):
        try:
            self.mpdclient.disconnect()
        except mpd.ConnectionError:
            pass
        GLib.timeout_add(1000, self.do_connect)

    def do_connect(self):
        try:
            self.mpdclient.connect(*self.mpdaddress)
        except socket.error:
            self.reconnect()
        else:
            self.statechanged = True
            GLib.io_add_watch(self.mpdclient, GLib.IO_OUT, self.handle_write)


def main():
    parser = argparse.ArgumentParser(parents=[onoff.dbusutils.dbus_options])
    args = parser.parse_args()
    DBusGMainLoop(set_as_default=True)
    proxy = onoff.dbusutils.get_dbus_proxy(args)
    watcher = MpdWatcher(proxy)
    GLib.MainLoop().run()

if __name__ == "__main__":
    main()
