#!/usr/bin/env python3
"""
A simpe client for an onoff device. If no command is given, the device is
temporarily activated. When a command is given, the device is activated, then
the command is run. Some time after the command finishes, the device is
released.
"""

import argparse
import os

from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib

from onoff.common import OnoffState
import onoff.dbusutils

def wait_for_signal(proxy, signal, desired_state):
    loop = GLib.MainLoop()
    def callback(st):
        if st == desired_state:
            loop.quit()
    proxy.connect_to_signal(signal, callback)
    loop.run()

def main():
    parser = argparse.ArgumentParser(parents=[onoff.dbusutils.dbus_options])
    parser.add_argument("--duration", type=int, default=10,
                        help="how long to activate the device in seconds "
                        "(default: %(default)d")
    parser.add_argument("command", nargs=argparse.REMAINDER,
                        help="a command to be executed with the device being"
                             "activated for the duration of the execution")
    parser.add_argument("--list", action="store_true",
                        help="list available devices and exit")
    args = parser.parse_args()
    DBusGMainLoop(set_as_default=True)
    if args.list:
        for elem in onoff.dbusutils.list_objects(args):
            print(elem)
    elif args.command:
        proxy = onoff.dbusutils.get_dbus_proxy(args)
        st, fd = proxy.activatefd()
        st = OnoffState(st)
        fd = fd.take()
        os.dup2(fd, 254)
        os.close(fd)
        if st != OnoffState.active:
            print("state is %s waiting for signal" % st.name)
            wait_for_signal(proxy, "changestate", OnoffState.active.value)
            print("new state is actived")
        os.execvp(args.command[0], args.command)
    else:
        proxy = onoff.dbusutils.get_dbus_proxy(args)
        st = OnoffState(proxy.activatetime(args.duration))
        if st != OnoffState.active:
            print("state is %s waiting for signal" % st.name)
            wait_for_signal(proxy, "changestate", OnoffState.active.value)
            print("new state is active")

if __name__ == "__main__":
    main()
